package com.project.ensusmanos.model.entity;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "ciudadano")
public class Ciudadano {

    @Id
    @Column(name = "identificacion_ciudadano")
    private long identificacion;

    @Column(name = "nombre_ciudadano")
    private String nombre;

    @Column(name = "apellido_ciudadano")
    private String apellido;

    @Column(name = "direccion_ciudadano")
    private String direccion;

    @Column(name = "ciudad")
    private String ciudad;

    @Column(name = "telefono")
    private long telefono;

    @OneToMany(mappedBy = "ciudadanos")
    private List<Evento> listaEventos;

    @OneToMany(mappedBy = "ciudadanosRegistro")
    private List<Registro> listaRegistros;

    public Ciudadano() {

    }

    public long getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(long identificacion) {
        this.identificacion = identificacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public long getTelefono() {
        return telefono;
    }

    public void setTelefono(long telefono) {
        this.telefono = telefono;
    }
}
