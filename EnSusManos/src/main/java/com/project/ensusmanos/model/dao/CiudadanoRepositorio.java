package com.project.ensusmanos.model.dao;

import com.project.ensusmanos.model.entity.Ciudadano;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class CiudadanoRepositorio {

    @Autowired
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public List<Ciudadano> getAllCiudadanos() {
        Criteria criteria = getSessionFactory().getCurrentSession().createCriteria(Ciudadano.class);
        return criteria.list();
    }

    public String crearCiudadano(Ciudadano ciudadano) {
        getSessionFactory().getCurrentSession().save(ciudadano);
        return ciudadano.getNombre();
    }

    public Ciudadano obtenerCiudadanoPorID(long identificacion) {
        Criteria criteria = getSessionFactory().getCurrentSession().createCriteria(Ciudadano.class);
        criteria.add(Restrictions.eq("identificacion", identificacion));
        return (Ciudadano) criteria.uniqueResult();
    }
}
