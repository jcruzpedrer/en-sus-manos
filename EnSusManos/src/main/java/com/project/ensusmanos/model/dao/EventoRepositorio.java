package com.project.ensusmanos.model.dao;

import com.project.ensusmanos.model.entity.Evento;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class EventoRepositorio {

    @Autowired
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public long crearEvento(Evento evento) {
        getSessionFactory().getCurrentSession().save(evento);
        return evento.getIdEvento();
    }

    public List<Evento> getAllEvento() {
        Criteria criteria = getSessionFactory().getCurrentSession().createCriteria(Evento.class);
        return criteria.list();
    }

    public List<Evento> getEventoCiudadano(long identificacion) {
        Session session = getSessionFactory().openSession();
        String sql = "SELECT * FROM EVENTO WHERE identificacion_ciudadano=" + identificacion;
        SQLQuery query = session.createSQLQuery(sql);
        query.addEntity(Evento.class);
        List results = query.list();
        return results;
    }

    public List<Evento> obtenerPuntajeEvento() {
        Criteria criteria = getSessionFactory().getCurrentSession().createCriteria(Evento.class);
        criteria.add(Restrictions.lt("puntaje", 10));
        criteria.addOrder(Order.desc("puntaje"));
        return criteria.list();
    }
}
