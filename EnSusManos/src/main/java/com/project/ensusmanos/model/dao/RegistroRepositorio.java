package com.project.ensusmanos.model.dao;

import com.project.ensusmanos.model.entity.Registro;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class RegistroRepositorio {

    @Autowired
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public long crearCiudadanoRegistro(Registro registro) {
        getSessionFactory().getCurrentSession().save(registro);
        return registro.getIdRegistro();
    }

    public List<Registro> obtenerLogin(String nombreUsuario, String contrasena) {
        Criteria criteria = getSessionFactory().getCurrentSession().createCriteria(Registro.class);
        criteria.add(Restrictions.eq("nombreUsuario", nombreUsuario));
        criteria.add(Restrictions.eq("contrasena", contrasena));
        return criteria.list();
    }
}
