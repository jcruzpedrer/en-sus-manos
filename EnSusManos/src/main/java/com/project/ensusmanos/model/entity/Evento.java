package com.project.ensusmanos.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "evento")
public class Evento {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_evento")
    private long idEvento;

    @Transient
    private long idCiudadano;

    @Column(name = "fecha_evento")
    private String fecha;

    @Column(name = "hora_evento")
    private String hora;

    @Column(name = "barrio_evento")
    private String barrio;

    @Column(name = "situacion")
    private String situacion;

    @Column(name = "puntaje")
    private int puntaje;

    @ManyToOne
    @JoinColumn(name = "identificacion_ciudadano")
    private Ciudadano ciudadanos;

    public Evento() {

    }

    public long getIdEvento() {
        return idEvento;
    }

    public void setIdEvento(long idEvento) {
        this.idEvento = idEvento;
    }

    public long getIdCiudadano() {
        return idCiudadano;
    }

    public void setIdCiudadano(long idCiudadano) {
        this.idCiudadano = idCiudadano;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getBarrio() {
        return barrio;
    }

    public void setBarrio(String barrio) {
        this.barrio = barrio;
    }

    public String getSituacion() {
        return situacion;
    }

    public void setSituacion(String situacion) {
        this.situacion = situacion;
    }

    public int getPuntaje() {
        return puntaje;
    }

    public void setPuntaje(int puntaje) {
        this.puntaje = puntaje;
    }

    public Ciudadano getCiudadanos() {
        return ciudadanos;
    }

    public void setCiudadanos(Ciudadano ciudadanos) {
        this.ciudadanos = ciudadanos;
    }
}
