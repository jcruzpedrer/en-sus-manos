package com.project.ensusmanos.controller;

import com.project.ensusmanos.model.dao.CiudadanoRepositorio;
import com.project.ensusmanos.model.dao.EventoRepositorio;
import com.project.ensusmanos.model.entity.Ciudadano;
import com.project.ensusmanos.model.entity.Evento;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class EventoServicio {

    @Autowired
    public EventoRepositorio eventoRepositorio;

    @Autowired
    public CiudadanoRepositorio ciudadanoRepositorio;

    public EventoServicio() {

    }

    public long crearEvento(Evento evento) {
        Ciudadano ciudadano = ciudadanoRepositorio.obtenerCiudadanoPorID(evento.getIdCiudadano());
        evento.setCiudadanos(ciudadano);
        return eventoRepositorio.crearEvento(evento);
    }

    public List<Evento> getAllEventos() {
        return eventoRepositorio.getAllEvento();
    }

    public List<Evento> getEventoCiudadano(long identificacion) {
        List<Evento> eventoC = eventoRepositorio.getEventoCiudadano(identificacion);
        return eventoC;
    }

    public List<Evento> obtenerPuntaje() {
        List<Evento> ordenarC = eventoRepositorio.obtenerPuntajeEvento();
        return ordenarC;
    }
}
