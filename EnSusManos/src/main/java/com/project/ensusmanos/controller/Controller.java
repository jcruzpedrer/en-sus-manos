package com.project.ensusmanos.controller;

import com.project.ensusmanos.model.entity.Ciudadano;
import com.project.ensusmanos.model.entity.Evento;
import com.project.ensusmanos.model.entity.Registro;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1")
public class Controller {

    @Autowired
    public CiudadanoServicio ciudadanoServicio;

    @Autowired
    public EventoServicio eventoServicio;

    @Autowired
    public RegistroServicio registroServicio;

    //Sercivio que permite crear ciudadanos
    @RequestMapping(value = "/ciudadanos", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> crearCiudadano(@RequestBody Ciudadano ciudadano) {
        String resultado = "Se creo un ciudadano con Nombre: " + ciudadanoServicio.crearCiudadano(ciudadano);
        return new ResponseEntity<String>(resultado, HttpStatus.OK);
    }

    //Servicio que permite registrar ciudadanos
    @RequestMapping(value = "/registros", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> crearRegistro(@RequestBody Registro registro) {
        String resultado = "Se creo registro con Id: " + registroServicio.crearRegistro(registro);
        return new ResponseEntity<String>(resultado, HttpStatus.OK);
    }

    //Servicio que trae todos los ciudadanos
    @RequestMapping(value = "/usuarios", method = RequestMethod.GET)
    public ResponseEntity<List<Ciudadano>> obtenerCiudadanos() {
        List<Ciudadano> ciudadano = ciudadanoServicio.getAllCiudadano();
        return new ResponseEntity<List<Ciudadano>>(ciudadano, HttpStatus.OK);
    }

    //Servicio que realiza el Login
    @RequestMapping(value = "/registros/login", method = RequestMethod.GET)
    public ResponseEntity<List<Registro>> loginCiudadanos(@RequestParam("nombreUsuario") String nombreUsuario, @RequestParam("contrasena") String contrasena) {
        List<Registro> registro = registroServicio.obtenerLogin(nombreUsuario, contrasena);
        return new ResponseEntity<List<Registro>>(registro, HttpStatus.OK);
    }

    //Servicio que busca usuario por Id
    @RequestMapping(value = "/ciudadanos/{id}", method = RequestMethod.GET)
    public ResponseEntity<Ciudadano> obtenerCiudadanoPorID(@PathVariable("id") long identificacion) {
        Ciudadano ciudadano = ciudadanoServicio.obtenerCiudadanoPorID(identificacion);
        return new ResponseEntity<Ciudadano>(ciudadano, HttpStatus.OK);
    }

    //Servicio que crea un evento
    @RequestMapping(value = "/eventos", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> crearEvento(@RequestBody Evento evento) {
        String resultado = "Se creo evento con Id:" + eventoServicio.crearEvento(evento);
        return new ResponseEntity<String>(resultado, HttpStatus.OK);
    }

    //Servicio que trae todos los eventos
    @RequestMapping(value = "/eventos", method = RequestMethod.GET)
    public ResponseEntity<List<Evento>> obtenerEventos() {
        List<Evento> evento = eventoServicio.getAllEventos();
        return new ResponseEntity<List<Evento>>(evento, HttpStatus.OK);
    }

    //Servicio que trae eventos por ciudadano
    @RequestMapping(value = "/eventos/ciudadanos/{id}", method = RequestMethod.GET)
    public ResponseEntity<List<Evento>> obtenerEventosCiudadanos(@PathVariable("id") long identificacion) {
        List<Evento> eventosC = eventoServicio.getEventoCiudadano(identificacion);
        return new ResponseEntity<List<Evento>>(eventosC, HttpStatus.OK);
    }

    //Servicio que trae que ordena el puntaje
    @RequestMapping(value = "/ciudadanos/eventos", method = RequestMethod.GET)
    public ResponseEntity<List<Evento>> ordenarCiudadano() {
        List<Evento> ordenarO = eventoServicio.obtenerPuntaje();
        return new ResponseEntity<List<Evento>>(ordenarO, HttpStatus.OK);
    }
}
