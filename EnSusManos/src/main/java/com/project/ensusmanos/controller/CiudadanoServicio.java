package com.project.ensusmanos.controller;

import com.project.ensusmanos.model.dao.CiudadanoRepositorio;
import com.project.ensusmanos.model.entity.Ciudadano;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CiudadanoServicio {

    @Autowired
    private CiudadanoRepositorio ciudadanoRepositorio;

    public CiudadanoServicio() {

    }

    public List<Ciudadano> getAllCiudadano() {
        return ciudadanoRepositorio.getAllCiudadanos();
    }

    public String crearCiudadano(Ciudadano ciudadano) {
        return ciudadanoRepositorio.crearCiudadano(ciudadano);
    }

    public Ciudadano obtenerCiudadanoPorID(long identificacion) {
        return ciudadanoRepositorio.obtenerCiudadanoPorID(identificacion);
    }
}
