package com.project.ensusmanos.controller;

import com.project.ensusmanos.model.dao.CiudadanoRepositorio;
import com.project.ensusmanos.model.dao.RegistroRepositorio;
import com.project.ensusmanos.model.entity.Ciudadano;
import com.project.ensusmanos.model.entity.Registro;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RegistroServicio {

    @Autowired
    private RegistroRepositorio registroRepositorio;

    @Autowired
    private CiudadanoRepositorio ciudadanoRepositorio;

    public RegistroServicio() {

    }

    public long crearRegistro(Registro registro) {
        Ciudadano ciudadano = ciudadanoRepositorio.obtenerCiudadanoPorID(registro.getIdCiudadano());
        registro.setCiudadanosRegistro(ciudadano);
        return registroRepositorio.crearCiudadanoRegistro(registro);
    }

    public List<Registro> obtenerLogin(String nombreUsuario, String contrasena) {
        return registroRepositorio.obtenerLogin(nombreUsuario, contrasena);
    }
}
